
module.exports = (sequelize, Sequelize) => {
    const Cliente = sequelize.define("Clientes", {

        cedula: {
            type: Sequelize.BIGINT,
            duplicating: false
        },
        nombre: {
            type: Sequelize.STRING
        },
        apellido: {
            type: Sequelize.STRING
        },
        id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        }
    }
    );



    return Cliente;
};
