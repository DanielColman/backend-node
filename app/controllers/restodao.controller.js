
const db = require("../models");
const Resto = db.Resto;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {


    // Validate request
    if (!req.body.nombre) {
        res.status(400).send({
            message: "Debe enviar nombre del Restaurant!"
        });
        return;
    }

    // Crea un Restaurant
    const resto = {
        nombre: req.body.nombre,
        direccion: req.body.direccion
    };

    // Guardamos a la base de datos
    Resto.create(resto)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al crear el restaurant."
            });
        });
};

// Update
exports.update = (req, res) => {
    const id = req.params.id;

    Resto.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "El Restaurant se actualizó correctamente."
                });
            } else {
                res.send({
                    message: `No se puede actualizar el Restaurant con id = ${id}. 
                    ¡Quizás no se encontró el Restaurant o el cuerpo requerido está vacío!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al actualizar el Restaurant con id=" + id
            });
        });
};

// Delete
exports.delete = (req, res) => {
    const id = req.params.id;

    Resto.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "El Restaurant se elimino correctamente."
                });
            } else {
                res.send({
                    message: `No se puede eliminar el Restaurant con id = ${id}. 
                    ¡Quizás no se encontró el Restaurant o el cuerpo requerido está vacío!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al eliminar el Restaurant con id=" + id
            });
        });
};


exports.findOne = (req, res) => {
    const id = req.params.id;
    Resto.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al obtener el Restaurant con id=" + id
            });
        });
};
exports.findAll = (req, res) => {
    const name = req.query.name;
    const condition = name ? { nombre: { [Op.iLike]: `%${name}%` } } : null;

    Resto.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ocurrio un error al obtener los Restaurants."
            });
        });
};

