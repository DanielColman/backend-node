const db = require("../models");
const Mesa = db.Mesa;
const Op = db.Sequelize.Op;
exports.create = (req, res) => {


    // Validate request
    if (!req.body.nombreMesa) {
        res.status(400).send({
            message: "Debe enviar nombre de la Mesa (Ej: Mesa Familiar)!"
        });
        return;
    }

    // Crea una Mea
    const mesa = {
        nombreMesa: req.body.nombreMesa,
        idResto: req.body.idResto,
        capacidad: req.body.capacidad,
        planta: req.body.planta,
        posX: req.body.posX,
        posY: req.body.posY
    };
    // Guardamos a la base de datos
    Mesa.create(mesa)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al crear la mesa."
            });
        });
};


// Update
exports.update = (req, res) => {
    const id = req.params.id;

    Mesa.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "La mesa se actualizó correctamente."
                });
            } else {
                res.send({
                    message: `No se puede actualizar la Mesa con id = ${id}. 
                    ¡Quizás no se encontró la Mesa o el cuerpo requerido está vacío!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al actualizar la Mesa con id=" + id
            });
        });
};

// Delete
exports.delete = (req, res) => {
    const id = req.params.id;

    Mesa.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "La mesa se elimino correctamente."
                });
            } else {
                res.send({
                    message: `No se puede eliminar la Mesa con id = ${id}. 
                    ¡Quizás no se encontró la Mesa o el cuerpo requerido está vacío!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al eliminar la Mesa con id=" + id
            });
        });
};

exports.findOne = (req, res) => {
    const id = req.params.id;
    Mesa.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al obtener la Mesa con id=" + id
            });
        });
};
exports.findAll = (req, res) => {
    const name = req.query.name;
    const condition = name ? { nombreMesa: { [Op.iLike]: `%${name}%` } } : null;

    Mesa.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ocurrio un error al obtener las mesas."
            });
        });
};

