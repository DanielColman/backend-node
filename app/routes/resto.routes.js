module.exports = app => {
    const resto = require("../controllers/restodao.controller.js");
    const router = require("express").Router();
    router.post("/", resto.create);
    router.put("/:id", resto.update);
    router.delete("/:id", resto.delete);
    router.get("/", resto.findAll);
    router.get("/:id", resto.findOne);
    app.use('/api/resto', router);
};
