module.exports = app => {
    const reserva = require("../controllers/reservadao.controller");
    const router = require("express").Router();
    router.post("/", reserva.create);
    router.put("/:id", reserva.update);
    router.delete("/:id", reserva.delete);
    router.get("/", reserva.findAll);
    router.get("/:id", reserva.findOne);
    app.use('/api/reserva', router);
};
